package main

// Sun AU format.
// https://en.wikipedia.org/wiki/Au_file_format

import (
	"encoding/binary"
	"io"
	"log"
)

type PulseAU struct {
	PulseRate  float64 // 3,500,000 for ZX Spectrum (CPU speed)
	SampleRate float64 // 22,050 Hz
	out        io.Writer
	level      int
	// number of pulses since last sample output
	cursor float64
}

func NewPulseAU(pulserate, samplerate int, out io.Writer) *PulseAU {
	if samplerate >= 500000 {
		log.Fatal("For safety reasons, the audio sample rate must be less than 500kHz; requested sample rate: ",
			samplerate)
	}
	binary.Write(out, binary.BigEndian, uint32(0x2e736e64))
	binary.Write(out, binary.BigEndian, uint32(24))
	binary.Write(out, binary.BigEndian, uint32(0xffffffff))
	binary.Write(out, binary.BigEndian, uint32(2))
	binary.Write(out, binary.BigEndian, uint32(samplerate))
	binary.Write(out, binary.BigEndian, uint32(2))
	return &PulseAU{float64(pulserate), float64(samplerate),
		out, 0, 0}
}

func (p *PulseAU) Pulse(n int) {
	cursor := float64(n) + p.cursor
	v := p.SampleLevel()

	samples_frac := cursor * float64(p.SampleRate) / float64(p.PulseRate)
	samples := int(samples_frac)

	AUSampleWrite(p.out, samples, v)

	p.level = 1 - p.level

	// remainder of the pulse, as a fraction of a audio sample
	samples_frac -= float64(samples)
	p.cursor = samples_frac / p.SampleRate * p.PulseRate
}

func (p *PulseAU) SampleLevel() int8 {
	// tzx2wav uses -3dB from full-rail. So why not here.
	if p.level == 0 {
		return -90
	} else {
		return 90
	}
}

func (p *PulseAU) PauseMilli(m int) {
	samples_frac := p.SampleRate * float64(m) / 1000.0
	samples := int(samples_frac)
	level := p.SampleLevel()
	AUSampleWrite(p.out, samples, level)
}

func AUSampleWrite(out io.Writer, samples int, v int8) error {
	buf := make([]int8, samples*2)
	for i := 0; i < samples; i++ {
		buf[2*i] = v
		buf[2*i+1] = -v
	}
	return binary.Write(out, binary.BigEndian, buf)
}

func (p *PulseAU) LowMilli(m int) {
	p.level = 0
	p.PauseMilli(m)
}
