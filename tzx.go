package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"regexp"
)

// Convert TAPE archive
// Specifications for the format used to be available on
// World Of Spectrum, but a 2020 reorganisation seems to have
// lost/moved them.
// archive.org to the rescue:
// https://web.archive.org/web/20161021004029/https://worldofspectrum.org/TZXformat.html

type TZXHeaderRecord struct {
	Signature    [8]byte
	Major, Minor uint8
}

type Tape struct {
	TZXHeaderRecord
	blocks []Block
}

func IsTZX(bs []byte) bool {
	signature := string(bs[:7])

	return signature == "ZXTape!"
}

func OpenTZX(bs []byte) (*Tape, error) {
	buf := bytes.NewBuffer(bs)

	header := TZXHeaderRecord{}
	binary.Read(buf, binary.LittleEndian, &header)

	signature := string(header.Signature[:7])

	if signature != "ZXTape!" {
		return nil, fmt.Errorf("Is this a ZXTape file? Signature is %s",
			signature)
	}

	tape := &Tape{TZXHeaderRecord: header}

	var err error
	tape.blocks, err = TapeBlocks(buf)
	return tape, err
}

type Block10HeaderRecord struct {
	Pause, Length uint16
}

type Block11HeaderRecord struct {
	PilotPulse uint16
	Sync       [2]uint16
	Zero, One  uint16
	PilotCount uint16
	LastBits   byte
	Pause      uint16
	Length     [3]byte
}

type Block14HeaderRecord struct {
	Zero, One uint16
	LastBits  byte
	Pause     uint16
	Length    [3]byte
}

type Block struct {
	ID int
	bs []byte
}

func TapeBlocks(r io.Reader) ([]Block, error) {
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	blocks := []Block{}
	var block_id byte
	var block_length int

	for {
		buf := bytes.NewBuffer(bs)
		err := binary.Read(buf, binary.LittleEndian, &block_id)
		if err == io.EOF {
			return blocks, nil
		} else if err != nil {
			return blocks, err
		}

		switch block_id {
		case 0x10:
			header := &Block10HeaderRecord{}
			binary.Read(buf, binary.LittleEndian, header)
			block_length = 5 + int(header.Length)

		case 0x11:
			header := &Block11HeaderRecord{}
			binary.Read(buf, binary.LittleEndian, header)
			data_length := LSBUint24(header.Length)
			block_length = 1 + binary.Size(header) +
				data_length

		case 0x12: // Pure Tone
			block_length = 5

		case 0x13: // Pulse Sequence
			l := bs[1]
			block_length = 2 + int(2*l)

		case 0x14: // Pure Data
			header := &Block14HeaderRecord{}
			binary.Read(buf, binary.LittleEndian, header)
			data_length := LSBUint24(header.Length)
			block_length = 1 + binary.Size(header) +
				data_length

		case 0x20:
			block_length = 3

		case 0x21: // Group Start
			name_length := int(bs[1])
			block_length = 2 + name_length

		case 0x22: // Group END
			block_length = 1

		case 0x2A: // 48K Stop The Tape
			block_length = 5

		case 0x30:
			var l byte
			binary.Read(buf, binary.LittleEndian, &l)
			block_length = 2 + int(l)

		case 0x32:
			var l uint16
			binary.Read(buf, binary.LittleEndian, &l)
			block_length = 3 + int(l)

		case 0x33:
			var n byte
			binary.Read(buf, binary.LittleEndian, &n)
			block_length = 2 + 3*int(n)

		case 0x5A:
			block_length = 10

		default:
			return nil, fmt.Errorf("Unknown Block ID %#x %v",
				block_id, bs[:32])
		}
		block := Block{int(bs[0]), bs[:block_length]}
		blocks = append(blocks, block)
		bs = bs[block_length:]
	}
}

func LSBUint24(bs [3]byte) int {
	return int(bs[0]) +
		int(bs[1])<<8 +
		int(bs[2])<<16
}

func IsTapeData(bs []byte) bool {
	switch bs[0] {
	case 0x10:
		return bs[5] == 0xff
	case 0x11:
		return true
	}
	return false
}

func TapeData(bs []byte) []byte {
	switch bs[0] {
	case 0x10:
		return bs[6 : len(bs)-1]
	case 0x11:
		return bs[0x12 : len(bs)-1]
	}
	log.Fatalf("Must not call TapeData() on Tape Block ID %#x", bs[0])
	return nil
}

func (block *Block) BlockPulses(p Pulser) {
	bs := block.bs

	switch block.ID {
	case 0x10:
		pause := int(binary.LittleEndian.Uint16(bs[1:3]))
		flag := bs[5]
		pilot := 8063 // 8064 in tzx2wav.c
		if flag&0x80 != 0 {
			pilot = 3223 // 3220 in tzx2wav.c
		}
		// These values are from the TZX spec
		// https://web.archive.org/web/20161021004029/https://worldofspectrum.org/TZXformat.html
		// tzx2wav.c has 885 instead of 855;
		// I suspect tzx2wav has a typo on the basis that
		// 855 * 2 == 1710
		TurboBlock(p, 2168, 667, 735, 855, 1710,
			pilot, 8,
			pause, bs[5:])

	case 0x11: // Turbo Speed Data
		header := &Block11HeaderRecord{}
		binary.Read(bytes.NewBuffer(bs[1:]),
			binary.LittleEndian, header)

		TurboBlock(p, int(header.PilotPulse),
			int(header.Sync[0]), int(header.Sync[1]),
			int(header.Zero), int(header.One),
			int(header.PilotCount),
			int(header.LastBits), int(header.Pause),
			bs[19:])

	case 0x12: // Pure Tone
		length := int(binary.LittleEndian.Uint16(bs[1:3]))
		count := int(binary.LittleEndian.Uint16(bs[3:5]))
		PulseTrain(p, length, count)

	case 0x13: // Pulse Sequence
		n := int(bs[1])
		for i := 0; i < n; i++ {
			length := int(binary.LittleEndian.Uint16(bs[i*2+2 : i*2+4]))
			PulseTrain(p, length, 1)
		}

	case 0x14: // Pure Data
		header := &Block14HeaderRecord{}
		binary.Read(bytes.NewBuffer(bs[1:]),
			binary.LittleEndian, header)
		TurboData(p, int(header.Zero), int(header.One),
			int(header.LastBits), int(header.Pause),
			bs[11:])

	case 0x20: // Pause
		millis := int(binary.LittleEndian.Uint16(bs[1:3]))
		p.PauseMilli(millis)

	case 0x21:
		group := string(bs[2:])
		fmt.Println("group: ", group)

	case 0x22: // Group end
		fmt.Println("  group end")

	case 0x2A: // Stop The Tape If In 48K Mode
		fmt.Println("Stop the tape (if in 48K mode)")

	case 0x30:
		description := string(bs[2:])
		fmt.Println("description: ", description)

	case 0x32: // Archive Info
		n := int(bs[3])
		PrintArchiveInfo(n, bs[4:])

	case 0x33: // Hardware Type
		n := int(bs[1])
		for i := 0; i < n; i++ {
			FormatHardware(bs[2+3*i : 5+3*i])
		}
	case 0x5A: // "Glue"
		// Just skip these.

	default:
		fmt.Printf("Unhandled block %#x [%d]\n",
			block.ID, len(block.bs))
	}
}

func PulseTrain(p Pulser, length, count int) {
	for count > 0 {
		p.Pulse(length)
		count -= 1
	}
}

// Pulse out a "turbo" block.
// May not actually be a turbo block, but it allows
// the parameters of a standard block to be changed.
func TurboBlock(p Pulser,
	ppulse, s1pulse, s2pulse, zpulse, opulse int,
	pilot int, last_bits int, pause int, bs []byte) {
	for pilot > 0 {
		p.Pulse(ppulse)
		pilot -= 1
	}
	p.Pulse(s1pulse)
	p.Pulse(s2pulse)
	TurboData(p, zpulse, opulse, last_bits, pause, bs)
}

func TurboData(p Pulser,
	zpulse, opulse, last_bits, pause int, bs []byte) {
	for len(bs) > 0 {
		bitcount := 8
		if len(bs) == 1 {
			bitcount = last_bits
		}
		D := bs[0]
		for bitcount > 0 {
			if D&0x80 == 0 {
				p.Pulse(zpulse)
				p.Pulse(zpulse)
			} else {
				p.Pulse(opulse)
				p.Pulse(opulse)
			}
			D <<= 1
			bitcount -= 1
		}
		bs = bs[1:]
	}
	if pause > 0 {
		p.PauseMilli(1)
		p.LowMilli(pause - 1)
	}
}

// bs is the block of bytes starting from
// the first text string.
func PrintArchiveInfo(n int, bs []byte) {
	fmt.Println("Archive Info")

	meta := []string{
		"Title",
		"Publisher",
		"Author",
		"Year",
		"Language",
		"Type",
		"Price",
		"Loader",
		"Origin",
		"Comment",
	}

	for n > 0 {
		key := bs[0]
		l := bs[1]
		text := string(bs[2 : 2+l])

		keystring := fmt.Sprint(key)
		if int(key) < len(meta) {
			keystring = meta[key]
		}

		fmt.Printf("%10s %s\n",
			keystring, CleanCR(text))

		bs = bs[2+l:]
		n -= 1
	}
}

// Clean the carriage returns in a string,
// converting everything to \n.
func CleanCR(in string) string {
	re := regexp.MustCompile(`\r\n|\n\r|\r`)
	return re.ReplaceAllString(in, "\n")
}

func FormatHardware(bs []byte) {
	fmt.Println(bs)
}
