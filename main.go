package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

var version string

var versionp = flag.Bool("version", false, "show version")

var offset = flag.Int("offset", 0, "Extract up to 96 glyphs from given byte offset")

func main() {
	flag.Parse()

	if *versionp {
		fmt.Println(version)
		os.Exit(0)
	}

	file_name := flag.Arg(0)

	// Open the input file.
	r, err := os.Open(file_name)
	if err != nil {
		log.Fatal(err)
	}
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		log.Fatal(err)
	}

	w, err := os.OpenFile("out.au",
		os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		log.Fatal(err)
	}
	defer w.Close()

	if IsTZX(bs) {
		au := NewPulseAU(3500000, 22050, w)
		err = TapeToPulse(au, bs)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		log.Fatalf("Input must be TAPE archive; it doesn't seem to be.")
	}
	if err != nil {
		log.Fatal(err)
	}
}

func TapeToPulse(p Pulser, bs []byte) error {
	tape, err := OpenTZX(bs)
	if err != nil {
		return err
	}

	fmt.Println(string(tape.Signature[:]), tape.Major, tape.Minor)

	last_id := 999
	count := 0

	for _, block := range tape.blocks {
		if block.ID == last_id {
			count += 1
		} else {
			if count != 0 {
				fmt.Printf("Block %#x count %d\n",
					last_id, count)
			}
			count = 1
			last_id = block.ID
		}
		block.BlockPulses(p)
	}
	fmt.Printf("Block %#x count %d\n",
		last_id, count)
	return nil
}

type PulsePrint struct {
	level int
}

// Invert and pulse
func (p *PulsePrint) Pulse(n int) {
	if p.level == 0 {
		p.level = 1
	} else {
		p.level = 0
	}
	fmt.Println(p.level, n)
}

func (p *PulsePrint) PauseMilli(n int) {
	fmt.Println("pause ", n, "ms")
}

func (p *PulsePrint) LowMilli(n int) {
	p.level = 0
	fmt.Println("low pause ", n, "ms")
}

type Pulser interface {
	Pulse(int)
	PauseMilli(int)
	LowMilli(int)
}
