tzaf: main.go tzx.go au.go
	go fmt
	go build -ldflags "-X main.version=$$(git describe --long --dirty --tags)"
