# TZX to audio file (.au file)

Usage

    tzaf game.tzx

Converts the `.tzx` file to a conventional audio file
(which is much larger).
The output file is `out.au`;
it is in Sun AU format (22050 samples/second, stereo, 8-bit PCM).

The stereo is coded so that Left + Right = 0.
This means that it can be played directly into the ZX Spectrum
MIC port using a stereo 3.5mm jack.

On macOS use `afplay` to play the audio file:

    afplay out.au

## How are blocks handled?

Blocks that encode tape pulses are simple:
the pulses are encoded in the selected output audio format.

Other blocks are mostly either ignored or cause their metadata
contents to be printed to `stdout`.


## Finding TZX files

The Internet Archive should have a good TOSEC ZX Spectrum set.
One was made available in 2020.


## Block Notes

- works: Bleepload Chaos\ \(1985\)\(Firebird\)\[BleepLoad\]\[re-release\].tzx
- Bleepload Dark\ Sceptre.tzx

- works: 0x13 Alien\ 8\ \(1985\)\(Ultimate\ Play\ The\ Game\).tzx
- 0x2A Dizzy\ 2\ -\ Treasure\ Island\ Dizzy\ \(1988\)\(Code\ Masters\)\(48K-128K\).tzx
  and Dizzy 3
- Dizzy 4, 5, 6, 7 ok
- 0x32 Chip\'s\ Challenge\ \(1991\)\(Erbe\)\(48K-128K\)\(ES\)\(en\)\(Side\ A\).tzx
- 0x33 AgentX

# END
